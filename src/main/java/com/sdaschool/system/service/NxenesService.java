package com.sdaschool.system.service;

import com.sdaschool.system.model.Nxenes;
import com.sdaschool.system.repository.NxenesRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NxenesService {

    private final NxenesRepository nxenesRepository;

    public NxenesService(NxenesRepository nxenesRepository) {
        this.nxenesRepository = nxenesRepository;
    }

    public List<Nxenes> findAll() {
        return nxenesRepository.findAll();
    }

    public Nxenes saveNxenes(Nxenes nxenes) {
        return nxenesRepository.save(nxenes);
    }

    public Nxenes findNxenesWithGivenId(Integer id) {
        return nxenesRepository.findById(id).orElse(null);
    }

    public List<Nxenes> findNxenesWithGivenEmer(String emer) {
       return nxenesRepository.findAllByEmer(emer);
    }
}
