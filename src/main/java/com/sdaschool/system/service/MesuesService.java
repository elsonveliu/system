package com.sdaschool.system.service;

import com.sdaschool.system.repository.MesuesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MesuesService {

    private final MesuesRepository mesuesRepository;

    public MesuesService(MesuesRepository mesuesRepository) {
        this.mesuesRepository = mesuesRepository;
    }
}
