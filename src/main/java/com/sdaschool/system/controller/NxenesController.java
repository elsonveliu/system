package com.sdaschool.system.controller;

import com.sdaschool.system.model.Nxenes;
import com.sdaschool.system.service.NxenesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/nxenes")
public class NxenesController {

    private final NxenesService nxenesService;

    public NxenesController(NxenesService nxenesService) {
        this.nxenesService = nxenesService;
    }

    @GetMapping("/")
    List<Nxenes> findAllNxenes(){
        return nxenesService.findAll();
    }


    @GetMapping("/{id}")
    Nxenes findNxenesById(@PathVariable Integer id){
        return nxenesService.findNxenesWithGivenId(id);
    }

    @GetMapping
    List<Nxenes> findNxenesByEmer(@RequestParam String emer){
        return nxenesService.findNxenesWithGivenEmer(emer);
    }

    @PostMapping("/")
    ResponseEntity<Nxenes> saveNxenes(@RequestBody Nxenes nxenes){
        return new ResponseEntity<>(nxenesService.saveNxenes(nxenes), HttpStatus.CREATED);
    }

}
