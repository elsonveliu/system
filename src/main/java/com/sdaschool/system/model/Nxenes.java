package com.sdaschool.system.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Nxenes {
    public Nxenes() {
    }

    public Nxenes(Integer id, String emer, String mbiemer) {
        this.id = id;
        this.emer = emer;
        this.mbiemer = mbiemer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmer() {
        return emer;
    }

    public void setEmer(String emer) {
        this.emer = emer;
    }

    public String getMbiemer() {
        return mbiemer;
    }

    public void setMbiemer(String mbiemer) {
        this.mbiemer = mbiemer;
    }

    @Id
    private Integer id;

    private String emer;
    private String mbiemer;
}
