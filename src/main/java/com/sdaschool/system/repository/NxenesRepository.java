package com.sdaschool.system.repository;

import com.sdaschool.system.model.Nxenes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NxenesRepository extends JpaRepository<Nxenes, Integer> {
    List<Nxenes> findAllByEmer(String emer);
}
