package com.sdaschool.system.repository;

import com.sdaschool.system.model.Mesues;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MesuesRepository extends JpaRepository<Mesues, Integer> {
}
